﻿(function () {
    'use strict';

    var controllerId = 'peopleController';

    angular.module('PeopleApp').controller(controllerId,
        ['$scope', 'peopleFactory', personController]);

    function personController($scope, personFactory) {
        $scope.people = [];

        personFactory.getPeople().success(function (data) {
            $scope.people = data;
        }).error(function (error) {
            console.log(error);
        });
    }
})();
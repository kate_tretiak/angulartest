﻿(function () {
    'use strict';

    var serviceId = 'peopleFactory';

    angular.module('PeopleApp').factory(serviceId,
        ['$http', personFactory]);

    function personFactory($http) {

        function getPeople() {
            return $http.get('/api/people');
        }

        var service = {
            getPeople: getPeople
        };

        return service;
    }
})();
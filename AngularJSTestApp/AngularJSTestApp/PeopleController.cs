﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace AngularJSTestApp
{
    public class PeopleController : ApiController
    {
        // GET api/<controller>
        [Route("api/people")]
        public JsonResult<string> Get()
        {
            List<DataRow> rows = Business.ProfileBus.GetAllProfiles().AsEnumerable().ToList();
            var profiles = new List<Common.ORM.Profiles>();
            foreach (DataRow item in rows)
            {
                profiles.Add(new Common.ORM.Profiles() { Id = Convert.ToInt32(item["ID"].ToString()), 
                    FirstName = item["FirstName"].ToString(), LastName = item["LastName"].ToString(), Username = item["UserName"].ToString() });                
            }

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string sJSON = oSerializer.Serialize(profiles);

            return Json(sJSON);
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}
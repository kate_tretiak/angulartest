﻿using AngularJSTestApplication.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace AngularJSTestApplication.Controllers
{
    [Route("api/people")]
    public class PeopleController : ApiController
    {
        // GET api/<controller>
        [HttpGet]
        public System.Web.Http.Results.JsonResult<List<People>> Get()
        {
            List<DataRow> rows = Business.ProfileBus.GetAllProfiles().AsEnumerable().ToList();
            List<People> profiles = new List<People>();

            foreach (DataRow item in rows)
            {
                if (Convert.ToBoolean(item["IfEnable"].ToString())) 
                {
                    profiles.Add(new People()
                    {
                        Id = Convert.ToInt32(item["ID"].ToString()),
                        FirstName = item["FirstName"].ToString(),
                        LastName = item["LastName"].ToString(),
                        UserName = item["UserName"].ToString()
                    });
                }   
            }

            return Json(profiles);
        }

        
        // GET api/<controller>/5
        [Route("api/people/{id}")]
        [HttpGet]
        public System.Web.Http.Results.JsonResult<People> Get(int id)
        {
            string[] pro = Business.ProfileBus.GetProfileByID(id.ToString()).Split(':');
            People profile = new People() 
            {
                Id = Convert.ToInt32(id),
                FirstName = pro[0],
                LastName = pro[1],
                UserName = pro[2]
            };

            return Json(profile);
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string data)
        {
            
        }

        // PUT api/<controller>/5
        [HttpPut]
        public void Put(int id, [FromBody]string value)
        {
            string[] profile = value.Split('&');
            for (int i = 0; i < profile.Length; i++)
            {
                profile[i] = profile[i].Substring(profile[i].IndexOf('=') + 1);
            }
 
            string[] pro = Business.ProfileBus.GetProfileByID(id.ToString()).Split(':');
            Common.ORM.Profiles oldprofile = Business.ProfileBus.GetProfileByUserName(pro[2]);
            oldprofile.FirstName = profile[1].Replace('+', ' ');
            oldprofile.LastName = profile[2].Replace('+', ' ');
            oldprofile.Username = profile[3].Replace('+', ' ');
            oldprofile.Save();
        }

        // DELETE api/<controller>/5
        [Route("api/people/{id}")]
        [HttpDelete]
        public void Delete(int id)
        {
            string[] pro = Business.ProfileBus.GetProfileByID(id.ToString()).Split(':');
            Common.ORM.Profiles profile = Business.ProfileBus.GetProfileByUserName(pro[2]);
            profile.Ifenable = false;
            profile.Save();
        }
    }
}
﻿
'use strict';
var app = angular.module('PersonsApp', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {
    // configure the routing rules here
    $routeProvider.when('/EditUserPage.html/:id', {
        templateUrl: "/EditUserPage.html",
        controller: 'editFormController'
    }).when('/', {
        templateUrl: "/UsersList.html",
        controller: 'personController'
    });

    // enable HTML5mode to disable hashbang urls
    $locationProvider.html5Mode(true);
})


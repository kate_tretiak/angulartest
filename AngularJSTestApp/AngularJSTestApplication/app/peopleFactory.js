﻿
    'use strict';

    var serviceId = 'personFactory';

    angular.module('PersonsApp').factory(serviceId,
        ['$http', personFactory]);

    function personFactory($http) {

        function getPeople() {
            return $http.get('/api/people');
        }

        function getPersonData(id) {
            return $http.get('/api/people/' + id);
        }

        function savePerson(person) {
            var data = $.param({
                "Id": person.Id,
                "FirstName": person.FirstName,
                "LastName": person.LastName,
                "UserName": person.UserName
            });

            data = JSON.stringify(data);
            var id = person.Id;
            return $http({
                headers:{'content-type': 'application/json'},
                url: '/api/people/?id='+id,
                method: "put",
                data: data
            });

            //return $http.post('/api/people', data);
        }

        function deletePerson(id) {
            return $http.delete('api/people/' + id);
        }

        var service = {
            getPeople: getPeople,
            getPersonData: getPersonData,
            savePerson: savePerson,
            deletePerson: deletePerson
        };

        return service;
    }

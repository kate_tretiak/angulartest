﻿'use strict'

var controllerId = 'editFormController';

angular.module('PersonsApp').controller(controllerId,
    ['$scope', 'personFactory', '$window', '$routeParams', editFormController]);

function editFormController($scope, personFactory, $window, $routeParams) {
    var id = $window.location.search.substring(4);
    debugger
    personFactory.getPersonData(id).success(function (data) {
        $scope.person = data;
    }).error(function (error) {
        console.log(error);
    });

    $scope.save = function (person) {
        //$params = $.param({
        //    "Id":person.Id,
        //    "FirstName": person.FirstName,
        //    "LastName": person.LastName,
        //    "UserName": person.UserName
        //})

        personFactory.savePerson(person).success(function () {
            $window.location.href = '/UsersList.html';
        }).error(function (error) {
            console.log(error);
        });
    }

    $scope.back = function () {
        $window.location.href = '/UsersList.html';
    }
}
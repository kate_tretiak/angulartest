﻿
    'use strict';

    var controllerId = 'personController';

    angular.module('PersonsApp').controller(controllerId,
        ['$scope', 'personFactory', '$window', '$filter', personController]);

    function personController($scope, personFactory, $window, $filter) {
        $scope.people = [];
        
        $scope.person;

        var loading_screen = pleaseWait({
            logo:'/Content/oie_transparent.png',
            backgroundColor: '#66FFCC',
            loadingHtml: "<div class='spinner'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div>"
        });

        personFactory.getPeople().success(function (data) {
            $scope.people = data;
            loading_screen.finish();
        }).error(function (error) {
            console.log(error);
        });

        $scope.getPerson = function (id) {
            personFactory.getPersonData(id).success(function (data) {
                $scope.person = data;
                $window.location.href = '/EditUserPage.html?id=' + id;
            }).error(function (error) {
                console.log(error);
            });
        }

        $scope.sortType = 'FirstName';
        $scope.sortReverse = false;

        $scope.delete = function (id, index) {
            personFactory.deletePerson(id).success(function () {
                $scope.people.splice(index, 1);
                $window.location.href = '/UsersList.html';
            }).error(function () {
                console.log(error);
            });
        }

        $scope.viewPerson = function (id) {
            personFactory.getPersonData(id).success(function (data) {
                $scope.person = data;
            }).error(function (error) {
                console.log(error);
            });
        }
    }


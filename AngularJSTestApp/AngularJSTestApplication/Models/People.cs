﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJSTestApplication.Models
{
    public class People
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
    }
}
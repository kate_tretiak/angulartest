﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AngularJSTestApplication.Controllers.Tests
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void GetTest()
        {
            PeopleController controller = new PeopleController();
            var result = controller.Get();

            Assert.IsNotNull(result);
        }
    }
}
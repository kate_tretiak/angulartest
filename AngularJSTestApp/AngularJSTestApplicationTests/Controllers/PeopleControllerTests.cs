﻿using Microsoft.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http.Results;

namespace AngularJSTestApplication.Controllers.Tests
{
    [TestClass]
    public class PeopleControllerTests
    {
        public PeopleController Controller
        {
            get
            {
                return new PeopleController();
            }
        }
        
        [TestMethod]
        public void GetShouldReturnNotNullTest()
        {
            ConnectionManager.AddConnection("Default", ConfigurationManager.ConnectionStrings["WorkConnectionString"].ConnectionString);
            var result = Controller.Get();
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(JsonResult<List<Models.People>>));
        }

        [TestMethod]
        public void GetByIdTest()
        {
            var result = Controller.Get(140);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(JsonResult<Models.People>));
        }

        [TestMethod]
        public void Delete()
        {
            Controller.Delete(153);
        }
    }
}